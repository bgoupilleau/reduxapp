var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

var app = express();

// view engine setup

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


//API
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/bookShop', {useMongoClient: true});

var db= mongoose.connection;
db.on('error', console.error.bind(console, '# MongoDB - connection error'))

app.use(session({
  secret: 'zferoijlmlkameirverijqsefer',
  saveUninitialized: false,
  resave: false,
  cookie: {maxAge: 1000*60*60*24*2},
  store: new MongoStore({mongooseConnection: db, ttl: 2*24*60*60})
}))

app.post('/cart', (req, res)=>{
  var cart = req.body;
  req.session.cart= cart;
  req.session.save((err)=>{
    if (err) {
      throw err;
    }
    res.json(req.session.cart);
  })
})

app.get('/cart', (req, res)=>{
  if (typeof req.session.cart !== undefined) {
    res.json(req.session.cart)
  }
})



var Book = require('./models/book');

//---->>> POST BOOKS <<<----
app.post('/books', (req, res)=>{
  var book= req.body;

  Book.create(book, (err, books)=>{
    if (err) {
      throw err;
    }
    res.json(books);
  })
})

//---->>> GET BOOKS <<<----
app.get('/books', (req, res)=>{
  Book.find((err, books)=>{
    if (err) {
      throw err;
    }
    return res.json(books);
  })
})
//---->>> DELETE BOOKS <<<----
app.delete('/books/:_id', (req, res)=>{
  var query ={_id: req.params._id}

  Book.remove(query, (err, books)=>{
    if (err) {
      console.log('#API server error', err);
    }
    return res.json(books);
  })
})

//---->>> UPDATE BOOKS <<<----
app.put('/books/:_id', (req, res)=>{
  var book = req.body;
  var query =req.params._id;

  var update ={
    '$set':{
      title:book.title,
      description: book.description,
      image: book.image,
      price: book.price
    }
  }
  var options={new:true}

  Book.findOneAndUpdate(query, update, options, (err, books)=>{
    if (err) {
      console.log('#API server error', err);
    }
    return res.json(books);
  })
})

//---->>> GET BOOKS IMAGE API <<<----
app.get('/images', (req, res)=>{
  const imgFolder = __dirname + '/public/images'
  const fs = require('fs');

  fs.readdir(imgFolder, (err, files)=>{
    if(err){
      return console.error(err);
    }
    const filesArr = [];
    files.forEach((file)=>{
      filesArr.push({name: file});
    })
    res.json(filesArr)
  })
})


//End API

app.listen(3001, (err)=>{
  if (err) {
    return console.log(err);
  }
  console.log('API server is listening on http://localhost:3001');
})
