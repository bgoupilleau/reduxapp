import React from 'react';
import Menu from './components/Menu';
import Footer from './components/Footer';

import {connect} from 'react-redux'
import {getCart} from '../src/actions/cartActions'

class Main extends React.Component{
  componentDidMount(){
    this.props.getCart();
  }
  render(){
    return (
      <div>
        <Menu cartItemNumber={this.props.totalQty}/>
        {this.props.children}
        <Footer />
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    totalQty: state.cart.totalQty
  }
}

export default connect(mapStateToProps, {getCart})(Main);
