import React from 'react';
import {MenuItem, Well, Panel, FormControl, FormGroup, ControlLabel, Button, InputGroup, DropdownButton, Image, Col, Row} from 'react-bootstrap';
import {connect} from 'react-redux';
import {findDOMNode} from 'react-dom';
import axios from 'axios';

import {postBook, deleteBook, getBooks, resetButton} from '../../actions/booksActions';

class BookForm extends React.Component{
  constructor(){
    super();
    this.state={
      images:[{}],
      img: ''
    }
  }
  componentDidMount(){
    this.props.getBooks();
    axios.get('/api/images').then((res)=>{
      this.setState({images: res.data})
    }).catch((err)=>{
      this.setState({images:'error loading image files', img: ''})
    })
  }
  handleSubmit(){
    const book=[{
      title: findDOMNode(this.refs.title).value,
      description: findDOMNode(this.refs.description).value,
      images: findDOMNode(this.refs.images).value,
      price: findDOMNode(this.refs.price).value
    }]
    this.props.postBook(book);
  }

  onDelete(){
      let bookId = findDOMNode(this.refs.delete).value;
      this.props.deleteBook(bookId)
  }
  handleSelect(img){
    this.setState({
      img: '/images/' + img
    })
  }
  resetForm(){
    this.props.resetButton();
    findDOMNode(this.refs.title).value = ''
    findDOMNode(this.refs.description).value =''
    findDOMNode(this.refs.price).value='';
    this.setState({img:''});
  }
  render(){
    const bookList=this.props.books.map((bookArr)=>{
      return (
        <option key={bookArr._id}> {bookArr._id}</option>
      )
    })
    const imgList = this.state.images.map((img, i)=>{
      return(
        <MenuItem key={i} eventKey={img.name}
          onClick={this.handleSelect.bind(this, img.name)}>{img.name}</MenuItem>
      )
    })

    return (
      <Well>
        <Row>
          <Col xs={12} sm={6}>
            <Panel>
              <InputGroup>
                <FormControl type="text" ref="images" value={this.state.img}/>
                <DropdownButton
                  componentClass={InputGroup.Button}
                  id="input-dropdown-addon"
                  title="Select an Image"
                  bsStyle="primary"
                  >
                  {imgList}
                </DropdownButton>
              </InputGroup>
              <Image src={this.state.img} responsive/>
            </Panel>
          </Col>
          <Col xs={12} sm={6}>
            <Panel>
              <FormGroup controlId='title' validationState={this.props.validation}>
                <ControlLabel>Title</ControlLabel>
                <FormControl
                  type='text'
                  placeholder='Enter title'
                  ref='title' />
                  <FormControl.Feedback/>
              </FormGroup>
              <FormGroup controlId='description' validationState={this.props.validation}>
                <ControlLabel>Description</ControlLabel>
                <FormControl
                  type='text'
                  placeholder='Enter description'
                  ref='description' />
                  <FormControl.Feedback/>
              </FormGroup>
              <FormGroup controlId='price' validationState={this.props.validation}>
                <ControlLabel>Price</ControlLabel>
                <FormControl
                  type='text'
                  placeholder='Enter price'
                  ref='price' />
                  <FormControl.Feedback/>
              </FormGroup>
              <Button onClick={(!this.props.msg)? (this.handleSubmit.bind(this)) :this.resetForm.bind(this)}
                bsStyle={ (!this.props.style)? ("primary"):(this.props.style) }>
                { (!this.props.msg)? ("Save"):(this.props.msg) }</Button>
            </Panel>
            <Panel style={{marginTop:'25px'}}>
              <FormGroup controlId="formControlsSelect">
                <ControlLabel>Select a book id to delete</ControlLabel>
                <FormControl ref="delete" componentClass="select" placeholder="select">
                  <option value="select">select</option>
                  {bookList}
                </FormControl>
              </FormGroup>
              <Button onClick={this.onDelete.bind(this)} bsStyle="danger">Delete book</Button>
            </Panel>
          </Col>
        </Row>

      </Well>
    )
  }
}

function mapStateToProps(state){
  return {
    books: state.books.books,
    msg: state.books.msg,
    style: state.books.style,
    validation: state.books.validation
  }
}

export default connect(mapStateToProps,{postBook, deleteBook, getBooks, resetButton})(BookForm);
