import axios from 'axios';

export function getCart(){
  return (dispatch)=>{
    axios.get('/api/cart').then((res)=>{
      dispatch({type: "GET_CART", payload: res.data})
    }).catch((err)=>{
      dispatch({type:"GET_CART_REJECTED", msg: 'error when getting the cart'})
    })
  }
}

export function addToCart(cart){
  return (dispatch)=>{
    axios.post('/api/cart', cart)
      .then((res)=>{
        dispatch({type: "ADD_TO_CART", payload: res.data})
      }).catch((err)=>{
        dispatch({type: "ADD_TO_CART_REJECTED", msg: 'error when adding to cart'})
      })
  }
}

export function deleteCartItem(cart){
  return (dispatch)=>{
    axios.post('/api/cart', cart)
      .then((res)=>{
        dispatch({type: "DELETE_CART_ITEM", payload: res.data})
      }).catch((err)=>{
        dispatch({type: "DELETE_CART_ITEM_REJECTED", msg: 'error when deleting item from cart'})
      })
  }
}

export function updateCart(_id, unit, cart){
  const indexToUpdate = cart.findIndex((book)=>{
    return book._id === _id;
  })
  const newcartToUpdate = {
    ...cart[indexToUpdate], quantity: cart[indexToUpdate].quantity + unit
  }
  let cartUpdated = [...cart.slice(0, indexToUpdate), newcartToUpdate, ...cart.slice(indexToUpdate+1)];
  return (dispatch)=>{
    axios.post('/api/cart', cartUpdated)
      .then((res)=>{
        dispatch({type: "UPDATE_CART", payload: res.data})
      }).catch((err)=>{
        dispatch({type: "UPDATE_CART_REJECTED", msg: 'error when adding to cart'})
      })
  }
}
