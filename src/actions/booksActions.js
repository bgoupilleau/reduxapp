import axios from 'axios';



export function getBooks(book){
  return function(dispatch){
    axios.get("/api/books").then((res)=>{
      dispatch({type: "GET_BOOKS", payload: res.data})
    }).catch((err)=>{
      dispatch({type:"GET_BOOKS_REJECTED", payload: err})
    })
  }
}

export function postBook(book){
  return function(dispatch){
    axios.post('/api/books', book).then((res)=>{
      dispatch({type: "POST_BOOK", payload: res.data})
    }).catch((err)=>{
      dispatch({type:"POST_BOOK_REJECTED", payload:"there was an error while posting new book"})
    })
  }
}

export function deleteBook(_id){
  return function(dispatch){
    axios.delete("/api/books/"+_id)
      .then((res)=>{
        dispatch({type: "DELETE_BOOK", payload: _id})
      }).catch((err)=>{
        dispatch({type:"DELETE_BOOK_REJECTED", payload: err})
      })
  }
}

export function updateBooks(book){
  return {
    type: "UPDATE_BOOK",
    payload: book
  }
}

export function resetButton(){
  return {
    type: "RESET_BUTTON"
  }
}
