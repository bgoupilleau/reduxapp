"use strict"
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';

import {applyMiddleware, createStore} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import BooksList from './components/pages/BooksList';
import Cart from './components/pages/Cart';
import BookForm from './components/pages/BookForm'
import Main from './Main'

import reducers from './reducers/index';
import {addToCart} from './actions/cartActions';
import {postBook, deleteBook, updateBooks} from './actions/booksActions'

const middleware = applyMiddleware(thunk, logger)
const store= createStore(reducers, middleware);

const Routes =(
  <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="/" component={Main}>
          <IndexRoute component={BooksList} />
          <Route path="/admin" component={BookForm}/>
          <Route path="/cart" component={Cart} />
        </Route>
      </Router>
  </Provider>
)

render(Routes, document.getElementById('app'));

// store.dispatch(postBook(
// ));
