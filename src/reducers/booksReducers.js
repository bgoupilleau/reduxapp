"use strict"

export function bookReducers (state={books:[]}, action){
  switch (action.type) {
    case "GET_BOOKS":
      return {...state, books:[...action.payload]}
      break;
    case "POST_BOOK":
      return {books:[...state.books, ...action.payload], msg:'Saved! Click to continue', style:'success', validation: 'success'};
      break;
    case "POST_BOOK_REJECTED":
      return {...state, msg:'Please, try again', style:'danger', validation:'error'};
      break;
    case "RESET_BUTTON":
      return {...state, msg:null, style:null, validation: null};
      break;
    case "DELETE_BOOK":
      const currentBooks = [...state.books];
      const filteredBooks = currentBooks.filter((book)=>{
        return book._id != action.payload;
      })
      return {books: [...filteredBooks]}
      break;

    case "UPDATE_BOOK":
      const currentBooksToUpdate = [...state.books];
      const indexToUpdate = currentBooksToUpdate.findIndex((book)=>{
        return book._id === action.payload._id;
      })
      const newBookToUpdate = {
        ...currentBooksToUpdate[indexToUpdate], title: action.payload.title
      }
      return {books: [...currentBooksToUpdate.slice(0, indexToUpdate), newBookToUpdate, ...currentBooksToUpdate.slice(indexToUpdate+1)]}

      // const currentBooksUp = [...state.books];
      // console.log(currentBooksUp);
      // const indexToUpdate = currentBooksUp.findIndex((book)=>{
      //   return book._id === action.payload._id;
      // })
      // console.log(currentBooksUp);
      // currentBooksUp[indexToUpdate].title= action.payload.title;
      // console.log(currentBooksUp);
      // return {books: [...currentBooksUp]}
      break;
  }
  return state;
}
